exports.USER_ROLES = {
  ADMIN: "Admin",
  USER: "User"
}

exports.CONTACT_TYPES = {
  GLOBAL: "Global",
  LOCAL: "Local"
}